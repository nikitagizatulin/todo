import React from 'react';
import { Provider } from 'react-redux'
import store from '../store';
import Todo from './todos/TodoMain';
import { createGlobalStyle } from 'styled-components';

const BodyStyle = createGlobalStyle`
body {
  background: #fff url('/images/retina_wood.png') top right repeat;
  margin: 0;
  padding: 0;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  }
`

function App() {
  return (
    <Provider store={store}>
      <>
        <BodyStyle />
        <Todo />
      </>
    </Provider>
  )
}

export default App;