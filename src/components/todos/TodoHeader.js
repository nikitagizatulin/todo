import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components'

const AddTodoImage = styled.img`cursor: pointer`

const TodoSmall = styled.small`
    text-align: right;
    display: block;
    font-size: 15px;
`
const TodoParagraph = styled.p`
    margin-top: 10px;
    margin-bottom: 10px;
    `
const TodoRow = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`
const TodoHeaderWrap = styled.div`
    padding: 10px;
    font-size: 24px;
    color: #fff;
    font-family: Arial;
    font-weight: bold;
    border-radius: 10px 10px 0 0;
    text-shadow: -1px -1px 1px #666;
    background: linear-gradient(to bottom,  #499bea 0%,#207ce5 100%); 
    `
const propTypes = {
    addTodo: PropTypes.func.isRequired,
    allCountTodo: PropTypes.number.isRequired
}

const TodoHeader = ({ addTodo, allCountTodo }) => (
    <TodoHeaderWrap>
        <TodoRow>
            <TodoParagraph>Todo list:</TodoParagraph>
            <AddTodoImage
                src="/images/add.png"
                alt="add todo"
                onClick={addTodo} />
        </TodoRow>
        <TodoSmall>Count of todo: <u>{allCountTodo}</u></TodoSmall>
    </TodoHeaderWrap>
)

TodoHeader.propTypes = propTypes

export default TodoHeader