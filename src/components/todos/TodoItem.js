import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components'
const TodoWrapper = styled.div`display:flex`
const ChecboxWrapper = styled.div`
    margin-right: 1px;
    padding: 10px 15px;
    height: 25px;
    border-right: 1px solid #ff0000;
    border-top: 1px solid #d9d9d9;
    position: relative;
    `
const CheckboxLabel = styled.label`
margin-top: 50%;
margin-left: 50%;
transform: translate(-50%,-50%);
width: 20px;
height: 20px;
cursor: pointer;
position: absolute;
top: 0;
left: 0;
background: #fcfff4;
background: linear-gradient(to bottom, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
border-radius: 4px;
box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
        &:after{
            content: '';
            width: 9px;
            height: 5px;
            position: absolute;
            top: 4px;
            left: 4px;
            border: 3px solid #333;
            border-top: none;
            border-right: none;
            background: transparent;
            opacity: 0;
            transform: rotate(-45deg);
            &:hover{
                opacity: 0.5;
            }
        }
`
const CheckboxInput = styled.input`
    visibility: hidden;
    &:checked + ${CheckboxLabel}:after{
        opacity: 1;
    }
`
const TodoInputWrapper = styled.span`
  height: 25px;
    border-left: 1px solid #ff0000;
    padding: 10px;
    border-top: 1px solid #d9d9d9;
    width:100%;
`
const TodoInput = styled.input`
    font-family: "Times New Roman";
    border: none;
    width: 100%;
    font-size: 18px;
    height: 24px;
    color: ${props => props.disabled ? "#d3d3d3" : "#333"};
    text-decoration: ${props => props.disabled ? "line-through" : "inherit"};
    font-style: ${props => props.disabled ? "italic" : "normal"};
    outline: none;
    &:checked{
        
    }
`
const DeleteButtonWrapper = styled.span`
display: flex;
align-items: center;
border-top: 1px solid #d9d9d9;
`
const DeleteButton = styled.span`
    cursor: pointer;
    color: rgb(207, 37, 7);
    font-size: 30px;
    display: inline-block;
    transform: rotate(45deg);
    user-select: none;
`
const propTypes = {
    todo: PropTypes.shape({
        id: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired
    }),
    todoStatusToggle: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    addTodo: PropTypes.func.isRequired,
    updateTodoValue: PropTypes.func.isRequired
}

const TodoItem = ({ todo, todoStatusToggle, deleteTodo, updateTodoValue, addTodo, autofocus }) => {
    let todoInput = null
    return (
        <TodoWrapper>
            <ChecboxWrapper>
                <CheckboxInput
                    type="checkbox"
                    name="status"
                    id={todo.id}
                    checked={todo.completed}
                    onChange={e => { todoStatusToggle(todo.id, todoInput.value) }} />
                <CheckboxLabel htmlFor={todo.id}></CheckboxLabel>
            </ChecboxWrapper>
            <TodoInputWrapper>
                <TodoInput
                    autoFocus={autofocus}
                    ref={input => todoInput = input}
                    type="text"
                    onKeyPress={(e) => { e.key === 'Enter' && addTodo() }}
                    onChange={e => { updateTodoValue(todo.id, e.target.value) }}
                    defaultValue={todo.value}
                    disabled={todo.completed}
                />
            </TodoInputWrapper>
            <DeleteButtonWrapper>
                <DeleteButton onClick={() => { deleteTodo(todo.id) }}>+</DeleteButton>
            </DeleteButtonWrapper>
        </TodoWrapper>
    )
}

TodoItem.propTypes = propTypes

export default TodoItem