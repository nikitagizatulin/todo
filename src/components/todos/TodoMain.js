import React, { Component } from 'react';
import { connect } from 'react-redux'
import TodoHeader from './TodoHeader'
import TodoItem from './TodoItem'
import Modal from 'react-responsive-modal'
import * as todoActions from '../../store/todos/actions'
import styled from 'styled-components'
import { debounce } from 'lodash'

const ModalText = styled.h2`color:rgb(214, 43, 43)`
const TodoMainWrap = styled.div`
    background: #fff;
    width: 50%;
    margin: 0 auto;
    margin-top: 150px;
    margin-bottom: 150px;
    box-shadow:         0px 0px 5px 3px #c3c3c3;
    border-radius: 10px; 
`

class Todo extends Component {
    state = {
        modalOpen: false,
        modalText: ''
    }
    onOpenModal = text => {
        this.setState({
            modalOpen: true,
            modalText: text
        })
    }

    onCloseModal = () => {
        this.setState({
            modalOpen: false,
            modalText: ''
        })
    }

    toggleStatus = (id, value) => {
        value.trim() !== '' ?
            this.props.updateTodoStatus(id) :
            this.onOpenModal('Cannot set status "complete" to empty field')
    }

    render() {
        const { todos, deleteTodo, updateTodoValue, addTodo } = this.props
        return (
            <TodoMainWrap>
                <TodoHeader
                    addTodo={addTodo}
                    allCountTodo={todos.todoList.length}
                />
                {todos.todoList.map((todo, index) => (
                    <TodoItem
                        autofocus={index === todos.todoList.length - 1}
                        todo={todo}
                        key={todo.id}
                        addTodo={addTodo}
                        updateTodoValue={debounce(updateTodoValue, 300)}
                        todoStatusToggle={this.toggleStatus}
                        deleteTodo={deleteTodo} />
                ))}
                <Modal
                    open={this.state.modalOpen}
                    onClose={this.onCloseModal}
                    styles={{
                        closeButton: {
                            top: 5,
                            right: 5,
                            userSelect: 'none',
                            cursor: 'pointer'
                        },
                        modal: {
                            borderRadius: 5
                        }
                    }}
                    center>
                    <ModalText>{this.state.modalText}</ModalText>
                </Modal>
            </TodoMainWrap>
        );
    }
}

const mapStateToProps = state => state
const mapActionsToProps = { ...todoActions }

export default connect(mapStateToProps, mapActionsToProps)(Todo);