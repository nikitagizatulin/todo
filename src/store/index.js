import { createStore, combineReducers, applyMiddleware } from 'redux';
import todosReducer from './todos/reducer';

import logger from './middlewares/logger'

const rootReducer = combineReducers({
  todos: todosReducer
})

const store = createStore(rootReducer, applyMiddleware(logger));

store.subscribe(() => {  localStorage.setItem('todos', JSON.stringify(store.getState().todos))})

export default store;