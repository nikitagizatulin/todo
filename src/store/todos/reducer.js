import { ADD_TODO, UPDATE_TODO_STATUS, DELETE_TODO, UPDATE_TODO_VALUE } from './actions'

const initialState = localStorage.getItem('todos') ? JSON.parse(localStorage.getItem('todos')) : { todoList: [] }

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todoList: [...state.todoList, action.payload.todo]
      }
    case DELETE_TODO:
      return {
        ...state,
        todoList: state.todoList.filter(todo => todo.id !== action.payload.todo.id)
      }
    case UPDATE_TODO_STATUS:
      return {
        ...state,
        todoList: state.todoList.map(todo => todo.id === action.payload.todo.id && (todo.completed = !todo.completed) ? todo : todo)
      }
    case UPDATE_TODO_VALUE:
      return {
        ...state,
        todoList: state.todoList.map(todo => todo.id === action.payload.todo.id && (todo.value = action.payload.todo.value) ? todo : todo)
      }
    default:
      return state
  }
}