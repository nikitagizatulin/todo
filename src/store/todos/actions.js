import uuidv4 from 'uuid'

export const ADD_TODO = 'ADD_TODO'
export const addTodo = () => ({
  type: ADD_TODO,
  payload: {
    todo: {
      id: uuidv4(),
      value: '',
      completed: false
    }
  }
})

export const UPDATE_TODO_VALUE = 'UPDATE_TODO_VALUE'
export const updateTodoValue = (id, value) => ({
  type: UPDATE_TODO_VALUE,
  payload: {
    todo: {
      id,
      value
    }
  }
})

export const UPDATE_TODO_STATUS = 'UPDATE_TODO_STATUS'
export const updateTodoStatus = id => ({
  type: UPDATE_TODO_STATUS,
  payload: {
    todo: {
      id
    }
  }
})

export const DELETE_TODO = 'DELETE_TODO'
export const deleteTodo = id => ({
  type: DELETE_TODO,
  payload: {
    todo: {
      id
    }
  }
})